from flask import Flask,render_template

app = Flask(__name__)


@app.route('/Admin_Home')
def hello_world():
    return render_template('Admin_Home_Page.html')
@app.route('/Admin_login')
def hello_login():
    return render_template('Admin_login.html')

@app.route('/Admin_college_mngt')
def hello_collegemgmt():
    return render_template('Admin_College_Management.html')

@app.route('/Admin_course_mngt')
def hello_coursemgmt():
    return render_template('Admin_Course_Management.html')

@app.route('/Admin_subject_mngt')
def hello_subjectmgmt():
    return render_template('Admin_Subject_Management.html')

@app.route('/Admin_view_college')
def hello_viewcollege():
    return render_template('Admin_View_College.html')

@app.route('/Admin_view_course')
def hello_viewcourse():
    return render_template('Admin_View_Course.html')

@app.route('/Admin_view_subject')
def hello_viewsubject():
    return render_template('Admin_View_Subject.html')

@app.route('/College_enroll_student')
def hello_enrollstudent():
    return render_template('College_Enroll_student.html')

@app.route('/College_home_page')
def hello_collegehome():
    return render_template('College_Home_Page.html')

@app.route('/College_registration')
def hello_collegereg():
    return render_template('College_Registration.html')

@app.route('/College_view')
def hello_collegeview():
    return render_template('College_View_College.html')




@app.route('/documents')
def hello_project():
    return 'security docs'


if __name__ == '__main__':
    app.run()
